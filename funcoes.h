/***********************************************************/
/*																												 */
/*																												 */
/*                 FUNCOES ESSENCIAIS - GALA  						 */
/*																												 */
/*																												 */
/***********************************************************/

/***********************************************************/
/*									Processos (sem retornos)							 */
/***********************************************************/

void inicializa_sistema();
void inicializaCli(Tentidade**);
void salvarCliente(Tentidade**);
void editaCliente(Tentidade**);
void inicializaAg(Tagenda**);
void salvarAgenda(Tagenda**);
void editaAgenda(Tagenda**);
void terminal();
void mensagem(char*,Tcor);
void abre_tela(tela);
void limpa_tela();
void espera_um_segundo();
void pula_linha();
void cabecalho(tipo_cabecalho);
void completa_texto(char,int);
void completa_com_borda_esquerda(char,char,int);
void completa_com_borda_direita(char,char,int,boolean);
void text(char*, boolean, boolean, int, boolean, Tajuste, Tcor);
void linha(boolean, boolean, int, boolean);
void logout_sistema();
void lerCliente();

/***********************************************************/
/*									Funcoes (com retornos) - INT			  	 */
/***********************************************************/

int dia();
int mes();
int ano();
int segundo();
int minuto();
int hora();
int diaS();
int diferencaValor(int, int);

/***********************************************************/
/*						  		Função de cores	em textos 						 */
/***********************************************************/

void fundo_cor(Tcor F);
void limpar();

/***********************************************************/
/*						  		Retorna ação do terminal  						 */
/***********************************************************/

char* modificatamanho(char*, Ttipotamanho);
char* datatostr(data_time);

Tentidade *pesquisaCli(Tentidade**, char**);
