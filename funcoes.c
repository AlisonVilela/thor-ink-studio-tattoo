#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "define.h"
#include "funcoes.h"

/***********************************************************/
/*																												 */
/*																												 */
/*               FUNCOES ESSENCIAIS - THOR INK  					 */
/*																												 */
/*																												 */
/***********************************************************/

/***********************************************************/
/*									Processos (sem retornos)							 */
/***********************************************************/
void inicializa_sistema()
{
	limpar();

	Olista = false;
	Ocad = false;
	Ovoltar = false;
	Osair = false;
	Ohelp = false;
	Ologout = false;
	Oinvalido = false;
	Oopcoes = false;
	Ocliente = false;
	OapagarCli = false;
	Oinvalidnome = false;
	Oinvalidrg = false;
	Oinvalidcpf = false;
	Oinvalidtelefone = false;
	Oinvalidcelular = false;
	Osalvocomsucesso = false;
	Oeditadocomsucesso = false;
	Opesquisa = false;
	Oerropesquisa = false;
	Omultpesquisa = false;
	Ofundolista = false;
	OpodeapagarCli = false;
	Oagenda = false;

	pesqencontrado[0] = '\0';
	pesqencontrado_cont = 0;

	dataatual.data.dia  = dia();
	dataatual.data.mes  = mes();
	dataatual.data.ano  = ano();
	dataatual.hora.sec  = segundo();
	dataatual.hora.min  = minuto();
	dataatual.hora.hora = hora();

	switch (diaS())
	{
		case 0:
			{
				strcpy(diadasemana, "Sabado");
			}
		case 1:
			{
				strcpy(diadasemana, "Domingo");
			}
		case 2:
			{
				strcpy(diadasemana, "Segunda-Feira");
			}
		case 3:
			{
				strcpy(diadasemana, "Terca-Feira");
			}
		case 4:
			{
				strcpy(diadasemana, "Quarta-Feira");
			}
		case 5:
			{
				strcpy(diadasemana, "Quinta-Feira");
			}
		case 6:
			{
				strcpy(diadasemana, "Sexta-Feira");
			}
	}

	erro = Vermelho;
	concluido = Verde;
	texto = Branco;
	corfundolista = Ciano;
	strcpy(bordac,vermelho);

	inicializaCli(&clientes);

	lerCliente();
	abre_tela(login);
}

void inicializaCli(Tentidade **iniCli)
{
	*iniCli = NULL;
}

void salvarCliente(Tentidade **iniCli)
{
	Tentidade *nCli;
	Tentidade *percorre;

	nCli = (Tentidade *) malloc(sizeof(Tentidade));
	strcpy(nCli -> nome, NovoCliente.nome);
	strcpy(nCli -> rg, NovoCliente.rg);
	strcpy(nCli -> cpf, NovoCliente.cpf);
	strcpy(nCli -> telefone, NovoCliente.telefone);
	strcpy(nCli -> celular, NovoCliente.celular);
	nCli -> Prox = NULL;

	if (*iniCli == NULL)
	{
		*iniCli = nCli;
	}
	else 
	{
		for (percorre = *iniCli; percorre -> Prox != NULL; percorre = percorre -> Prox);
    percorre -> Prox = nCli;
	}
}

void editaCliente(Tentidade **Cli)
{
	strcpy((*Cli) -> nome, NovoCliente.nome);
	strcpy((*Cli) -> rg, NovoCliente.rg);
	strcpy((*Cli) -> cpf, NovoCliente.cpf);
	strcpy((*Cli) -> telefone, NovoCliente.telefone);
	strcpy((*Cli) -> celular, NovoCliente.celular);
}

void inicializaAg(Tagenda **iniAg)
{
	*iniAg = NULL;
}

void salvarAgenda(Tagenda **iniAg)
{
	Tagenda *nAg;
	Tagenda *percorre;

	nAg = (Tagenda *) malloc(sizeof(Tagenda));
	strcpy(nAg -> desc, NovaAgenda.desc);
	nAg -> data = NovaAgenda.data;
	nAg -> cli = NovaAgenda.cli;	
	nAg -> Prox = NULL;

	if (*iniAg == NULL)
	{
		*iniAg = nAg;
	}
	else 
	{
		for (percorre = *iniAg; percorre -> Prox != NULL; percorre = percorre -> Prox);
    percorre -> Prox = nAg;
	}
}

void editaAgenda(Tagenda **Ag)
{
	strcpy((*Ag) -> desc, NovaAgenda.desc);
	(*Ag) -> data = NovaAgenda.data;
	(*Ag) -> cli = NovaAgenda.cli;
}

Tentidade *pesquisaCli(Tentidade **Cli, char **Text)
{
	Tentidade *percorre;
	Tentidade *encontrado;
	pesqencontrado_cont = 0;	
	char num;

	for (percorre = *Cli; percorre != NULL; percorre = percorre -> Prox)
	{
		if (strncmp(modificatamanho(*Text, maiusculo), modificatamanho(percorre -> nome, maiusculo), strlen(*Text)) == 0)
		{
			encontrado = percorre;
			pesqencontrado_cont++;
			if (strlen(*Text) != strlen(percorre -> nome))
			{
				strcat(pesqencontrado, percorre -> nome);
				strcat(pesqencontrado, "\n ");
			}
			else
			{
				pesqencontrado_cont = 1;
				break;
			}
		}
	}

	if (pesqencontrado_cont == 1)
	{
		strcpy(NovoCliente.nome, encontrado -> nome);
		strcpy(NovoCliente.rg, encontrado -> rg);
		strcpy(NovoCliente.cpf, encontrado -> cpf);
		strcpy(NovoCliente.telefone, encontrado -> telefone);
		strcpy(NovoCliente.celular, encontrado -> celular);
		
		return encontrado;	
	}
	else
	{
		return NULL;
	}
}

void abre_tela(tela T)
{
	switch (T)
	{
		case login:
			{
				pub_tela_aberta = login;
				loginTela(true);
				break;
			}
		case menu:
			{
				pub_tela_aberta = menu;
				menuTela();
				break;
			}
		case cliente:
			{
				pub_tela_aberta = cliente;
				clienteTela();
				break;
			}
		case cadastro:
			{
				pub_tela_aberta = cadastro;
				cadastroTela();
				break;
			}
		case lista:
			{
				pub_tela_aberta = lista;
				listarTela();
				break;
			}
		case agenda:
			{
				pub_tela_aberta = agenda;
				agendaTela(dataatual.data);
				break;
			}
	}
}

void mensagem(char *msg, Tcor cor)
{
	switch (cor)
	{
		case Preto:
			{
				printf("%s", preto);
				break;
			}
		case Vermelho:
			{
				printf("%s", vermelho);
				break;
			}
		case Verde:
			{
				printf("%s", verde);
				break;
			}
		case Marrom:
			{
				printf("%s", marrom);
				break;
			}
		case Azul:
			{
				printf("%s", azul);
				break;
			}
		case Roxo:
			{
				printf("%s", roxo);
				break;
			}
		case Ciano:
			{
				printf("%s", ciano);
				break;
			}
		case Cinza_Claro:
			{
				printf("%s", cinza_claro);
				break;
			}
		case Preto_Acinzentado:
			{
				printf("%s", preto_acinzentado);
				break;
			}
		case Vermelho_Claro:
			{
				printf("%s", vermelho_claro);
				break;
			}
		case Verde_Claro:
			{
				printf("%s", verde_claro);
				break;
			}
		case Amarelo:
			{
				printf("%s", amarelo);
				break;
			}
		case Azul_Claro:
			{
				printf("%s", azul_claro);
				break;
			}
		case Roxo_Claro:
			{
				printf("%s", roxo_claro);
				break;
			}
		case Ciano_Claro:
			{
				printf("%s", ciano_claro);
				break;
			}
		case Branco:
			{
				printf("%s", branco);
				break;
			}
		default:
			{
				limpar();
			}
	}
	printf("%s", msg);
	limpar();
}

void limpa_tela()
{
	system("clear");
}

void espera_um_segundo()
{
  clock_t temp;
  temp = clock () + CLOCKS_PER_SEC ;
  while (clock() < temp) {}
}

/***********************************************************/
/*									Funcoes (com retornos) - INT			  	 */
/***********************************************************/
int dia()
{
	struct tm *local;
	time_t t;
	t = time(NULL);
	local = localtime(&t);

  return (local->tm_mday);
}

int mes()
{
	struct tm *local;
	time_t t;
	t = time(NULL);
	local = localtime(&t);
  return (local->tm_mon+1);
}

int ano()
{
	struct tm *local;
	time_t t;
	t = time(NULL);
	local = localtime(&t);
  return (local->tm_year+1900);
}

int segundo()
{
	struct tm *local;
	time_t t;
	t = time(NULL);
	local = localtime(&t);
  return (local->tm_sec);
}

int minuto()
{
	struct tm *local;
	time_t t;
	t = time(NULL);
	local = localtime(&t);
  return (local->tm_min);
}

int hora()
{
	struct tm *local;
	time_t t;
	t = time(NULL);
	local = localtime(&t);
  return (local->tm_hour);
}

int diaS()
{
	struct tm *local;
	time_t t;
	t = time(NULL);
	local = localtime(&t);
  return (local->tm_wday);
}

int diferencaValor(int A, int B)
{
	int resultado;
	
	resultado = A - B;
	
	if (resultado < 0)
	{
		resultado = resultado * (-1);
	}
	
	return resultado;
}

/***********************************************************/
/*								 Função de cores em textos  						 */
/***********************************************************/

void fundo_cor(Tcor F)
{
	switch (F)
	{
		case Preto:
			{
				printf("%c[0;40m",0x1b);
				break;
			}
		case Vermelho:
			{
				printf("%c[0;41m",0x1b);
				break;
			}
		case Verde:
			{
				printf("%c[0;42m",0x1b);
				break;
			}
		case Marrom:
			{
				printf("%c[0;43m",0x1b);
				break;
			}
		case Azul:
			{
				printf("%c[0;44m",0x1b);
				break;
			}
		case Roxo:
			{
				printf("%c[0;45m",0x1b);
				break;
			}
		case Ciano:
			{
				printf("%c[0;46m",0x1b);
				break;
			}
		case Branco:
			{
				printf("%c[0;47m",0x1b);
				break;
			}
	}
}

void cabecalho(tipo_cabecalho TC)
{
	switch (TC)
	{
		case login_c:
			{
				pula_linha();
				linha(false, false,tam_tela,true);
				text("",true,true,tam_tela,true,centro,texto);
				text("",true,true,tam_tela,true,centro,texto);
				text("",true,true,tam_tela,true,centro,texto);
				text(" _______ _                  _____       _    ",true,true,tam_tela,true,centro,texto);
				text("|__   __| |                |_   _|     | |   ",true,true,tam_tela,true,centro,texto);
				text("   | |  | |__   ___  _ __    | |  _ __ | | __",true,true,tam_tela,true,centro,texto);
				text("   | |  | '_ \\ / _ \\| '__|   | | | '_ \\| |/ /",true,true,tam_tela,true,centro,texto);		
				text("   | |  | | | | (_) | |     _| |_| | | |   < ",true,true,tam_tela,true,centro,texto);
				text("   |_|  |_| |_|\\___/|_|    |_____|_| |_|_|\\_\\",true,true,tam_tela,true,centro,texto);
				text("                               Tattoo Studio.",true,true,tam_tela,true,centro,texto);
				text("",true,true,tam_tela,true,centro,texto);
				text("",true,true,tam_tela,true,centro,texto);
				linha(true, true,tam_tela, true);
				text("",true,true,tam_tela,true,centro,texto);
				text("Login",true,true,tam_tela,true,centro,texto);
				text("",true,true,tam_tela,true,centro,texto);
				linha(true, true,tam_tela, true);
				break;
			}
		case padrao:
			{
				pula_linha();
				linha(false, false,tam_tela, true);
				text(" _______ _                  _____       _    ",true,true,50,false,centro,texto);
				text(diadasemana,true,true,tam_tela - 52,true,direita,texto);
				text("|__   __| |                |_   _|     | |   ",true,true,50,false,centro,texto);
				text("",true,true,tam_tela - 52,true,centro,texto);
				text("   | |  | |__   ___  _ __    | |  _ __ | | __",true,true,50,false,centro,texto);
				text("",true,true,tam_tela - 52,true,centro,texto);
				text("   | |  | '_ \\ / _ \\| '__|   | | | '_ \\| |/ /",true,true,50,false,centro,texto);
				text("BEM VINDO",true,true,tam_tela - 52,true,centro,texto);
				text("   | |  | | | | (_) | |     _| |_| | | |   < ",true,true,50,false,centro,texto);
				text("",true,true,tam_tela - 52,true,centro,texto);
				text("   |_|  |_| |_|\\___/|_|    |_____|_| |_|_|\\_\\",true,true,50,false,centro,texto);
				text("",true,true,tam_tela - 52,true,centro,texto);
				text("                               Tattoo Studio.",true,true,50,false,centro,texto);
				text("",true,true,tam_tela - 52,true,centro,texto);
				linha(true, true,50, false);
				linha(true, true,tam_tela - 52, true);
				break;
			}	
	}
}

void completa_texto(char caracter,int num)
{
	int i;
	if (Ofundolista)
	{
		fundo_cor(corfundolista);
	}
	for (i = 0; i < num; i++)
	{
		printf("%c", caracter);
	}
}

void completa_com_borda_esquerda(char borda,char caracter,int num)
{
	if ((borda == ' ') && (Ofundolista))
	{
		fundo_cor(corfundolista);
		printf("%c", borda);
	}
	else
	{
		printf("%s%c", bordac,borda);
	}
	completa_texto(caracter,num);
}

void completa_com_borda_direita(char borda,char caracter,int num, boolean pula)
{
	completa_texto(caracter,num);
	if ((borda == ' ') && (Ofundolista))
	{
		fundo_cor(corfundolista);
		printf("%c", borda);
	}
	else
	{
		printf("%s%c", bordac,borda);
	}
	if (pula)
	{
		pula_linha();
	}
}

void pula_linha()
{
	printf("\n");
}

void linha(boolean esquerda, boolean direita, int tamanho, boolean pula)
{
	if (esquerda == true)
	{
		completa_com_borda_esquerda('|','_',tamanho/2);				
	}
	else
	{
		completa_com_borda_esquerda(' ','_',tamanho/2);				
	}
	if (direita == true)
	{
		completa_com_borda_direita('|','_',tamanho/2, pula);				
		
	}
	else
	{
		completa_com_borda_direita(' ','_',tamanho/2, pula);				
	}
}

void text(char *Mensagem, boolean Esquerda, boolean Direita, int Tamanho, boolean Pula_Linha, Tajuste ajuste, Tcor cor)
{
	int A, B;
	char E;
	char D;
	int Tamanho_Mensagem;

	Tamanho_Mensagem = strlen(Mensagem);

	A = (Tamanho / 2) - (Tamanho_Mensagem / 2);
	B = Tamanho - (A + Tamanho_Mensagem);
	
	if (Esquerda == true)
	{
		E = '|';
	}
	else
	{
		E = ' ';
	}
	if (Direita == true)
	{
		D = '|';
	}
	else
	{
		D = ' ';
	}

	if (ajuste == centro)
	{
		completa_com_borda_esquerda(E,' ',A);
		if (Ofundolista)
		{
			fundo_cor(corfundolista);
		}		
		mensagem(Mensagem, cor);				
		completa_com_borda_direita(D,' ',B,Pula_Linha);
	}
	if (ajuste == esquerda)
	{
		if (Esquerda == true)
		{
			printf("%c", '|');
		}
		else
		{
			if (Ofundolista)
			{
				fundo_cor(corfundolista);
			}		
			printf("%c", ' ');			
		}
		if (Ofundolista)
		{
			fundo_cor(corfundolista);
		}		
		mensagem(Mensagem, cor);
		completa_com_borda_esquerda(' ',' ',A - 1);				
		completa_com_borda_direita(D,' ',B,Pula_Linha);
	}
	if (ajuste == direita)
	{
		completa_com_borda_esquerda(E,' ',(A+B)-1);	
		if (Ofundolista)
		{
			fundo_cor(corfundolista);
		}
		mensagem(Mensagem, cor);			
		completa_com_borda_direita(D,' ',1,false);
		if (Pula_Linha == true)
		{
			pula_linha();
		}
	}
}

void limpar()
{
	printf("\033[0m");
}

void logout_sistema()
{
	abre_tela(login);
}

char* modificatamanho(char *texto, Ttipotamanho tipo)
{
	char *msg;
	int i;

	msg = malloc(sizeof (char)*strlen(texto)+1);
	for (i = 0; i < strlen(texto); i++)
	{	
		switch (tipo)
		{
			case maiusculo:
				{
					msg[i] = toupper(texto[i]);
					break;
				}
			case minusculo:
				{
					msg[i] = tolower(texto[i]);
					break;
				}
			default:
				{
					printf("ERRO");
				}
		}
	}
	msg[i] = '\0';
	return msg;
}
void lerCliente()
{
	FILE *cli;
	char Arq[tam];
	char *Insert;

  cli = fopen ("source/cli.dbt", "r");

	if (cli != NULL)
	{
		while (fgets(Arq, tam, cli) != NULL)
		{
			Insert = strtok(Arq,"|");

			strcpy(NovoCliente.nome,Insert);
			Insert = strtok(NULL,"|");
			strcpy(NovoCliente.rg,Insert);
			Insert = strtok(NULL,"|");
			strcpy(NovoCliente.cpf,Insert);
			Insert = strtok(NULL,"|");
			strcpy(NovoCliente.telefone,Insert);
			Insert = strtok(NULL,"|");
			Insert[strlen(Insert)-1] = '\0';
			strcpy(NovoCliente.celular,Insert);
			Insert = strtok(NULL,"|");
			salvarCliente(&clientes);
			NovoCliente.nome[0] = '\0';
			NovoCliente.rg[0] = '\0';
			NovoCliente.cpf[0] = '\0';
			NovoCliente.telefone[0] = '\0';
			NovoCliente.celular[0] = '\0';
		}
		
	}

	fclose(cli);
}

void gravarCliente()
{
	FILE *cli;
	Tentidade *percorre;

  cli = fopen("source/cli.dbt", "wt");

  if (cli != NULL)
  {
		for (percorre = clientes; percorre != NULL;percorre = percorre -> Prox)
  	{
			if (percorre -> nome[0] != '\0')
			{
				fprintf(cli,"%s|%s|%s|%s|%s\n",percorre -> nome, percorre -> rg, percorre -> cpf, percorre -> telefone, percorre -> celular);
			}
  	}
	}
  fclose(cli);
}
char* datatostr(data_time datatime)
{
	char i, i2;
	char num[4];
	char *str;

	str = (char *) malloc(sizeof(char)*10);
	str[0] = '\0';
	sprintf(num, "%d", datatime.dia);
	if (datatime.dia > 9)
	{
		strcat(str, "0");
	}
	strcat(str, num);
	strcat(str, "/");
	sprintf(num, "%d", datatime.mes);
	if (datatime.dia > 9)
	{
		strcat(str, "0");
	}
	strcat(str, num);
	strcat(str, "/");
	sprintf(num, "%d", datatime.ano);
	if (datatime.dia > 9)
	{
		strcat(str, "0");
	}
	strcat(str, num);

	return str;
}
