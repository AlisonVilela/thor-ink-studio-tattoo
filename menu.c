#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "define.h"
#include "funcoes.h"
#include "menu.h"

/***********************************************************/
/*																												 */
/*																												 */
/*                 		 MENU - THOR INK 	  	  						 */
/*																												 */
/*																												 */
/***********************************************************/

void menuTela()
{
	limpa_tela();

	cabecalho(padrao);
	text("",true,true,tam_tela,true,centro,texto);
	text("Menu",true,true,tam_tela,true,centro,texto);
	linha(true, true,tam_tela, true);
	text("",true,true,tam_tela,true,centro,texto);
	text("",true,true,tam_tela,true,centro,texto);
	text("Agenda",true,true,tam_tela,true,centro,texto);
	text("Cliente",true,true,tam_tela,true,centro,texto);
	text("Sair",true,true,tam_tela,true,centro,texto);
	text("",true,true,tam_tela,true,centro,texto);
	linha(true, true,tam_tela, true);
	pula_linha();
	limpar();
	terminal();
}
