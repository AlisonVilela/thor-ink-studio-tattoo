#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "define.h"
#include "funcoes.h"
#include "listar.h"

/***********************************************************/
/*																												 */
/*																												 */
/*                 Listar Cli - THOR INK   	  						 */
/*																												 */
/*																												 */
/***********************************************************/

void listarTela()
{
	limpa_tela();
	boolean comcor;
	comcor = true;	
	Tentidade *cliLista;

	cliLista = clientes;

	cabecalho(padrao);
	text("",true,true,tam_tela,true,centro,texto);
	text("Lista de Clientes",true,true,tam_tela,true,centro,texto);
	linha(true, true,tam_tela, true);
	text("",true,true,tam_tela,true,centro,texto);
	text("",true,true,tam_tela,true,centro,texto);

	text("Nome",true,false,52,false,centro,texto);
	text("Rg",false,false,20,false,centro,texto);
	text("Cpf",false,false,20,false,centro,texto);
	text("Telefone",false,false,20,false,centro,texto);
	text("Celular",false,true,20,true,centro,texto);
	linha(true, true,tam_tela, true);
	text("",true,true,tam_tela,true,centro,texto);
	if (cliLista == NULL)
	{
		text("",true,true,tam_tela,true,centro,texto);
		text("LISTA VAZIA!",true,true,tam_tela,true,centro,texto);
	}
	else
	{
		for (cliLista = clientes; cliLista != NULL; cliLista = cliLista -> Prox)
		{
			if (comcor)
			{
				Ofundolista = true;
				comcor = false;
			}
			else
			{
				Ofundolista = false;
				comcor = true;
			}
			text(cliLista -> nome,true,false,52,false,esquerda,texto);
			text(cliLista -> rg,false,false,20,false,centro,texto);
			text(cliLista -> cpf,false,false,20,false,centro,texto);
			text(cliLista -> telefone,false,false,20,false,centro,texto);
			text(cliLista -> celular,false,true,20,true,centro,texto);
			Ofundolista = false;
		}
	}

	linha(true, true,tam_tela, true);
	pula_linha();
	limpar();
	terminal();
}
