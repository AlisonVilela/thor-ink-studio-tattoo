#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "define.h"
#include "funcoes.h"
#include "cliente.h"

/***********************************************************/
/*																												 */
/*																												 */
/*                 Clientes - THOR INK 	  	  						 */
/*																												 */
/*																												 */
/***********************************************************/

void clienteTela()
{
	limpa_tela();

	cabecalho(padrao);
	text("",true,true,tam_tela,true,centro,texto);
	text("Clientes",true,true,tam_tela,true,centro,texto);
	linha(true, true,tam_tela, true);
	text("",true,true,tam_tela,true,centro,texto);
	text("",true,true,tam_tela,true,centro,texto);
	text("Cadastro",true,true,tam_tela,true,centro,texto);
	text("Listar",true,true,tam_tela,true,centro,texto);
	text("Voltar",true,true,tam_tela,true,centro,texto);
	text("",true,true,tam_tela,true,centro,texto);
	linha(true, true,tam_tela, true);
	pula_linha();
	limpar();
	terminal();
}
