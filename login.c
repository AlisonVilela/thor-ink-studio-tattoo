#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "define.h"
#include "login.h"
#include "funcoes.h"

/***********************************************************/
/*																												 */
/*																												 */
/*                 				LOGIN - GALA				 						 */
/*																												 */
/*																												 */
/***********************************************************/

void loginTela(boolean errado)
{
	char *password;
	char digito_usuario[256] = {0};
	char usuario[] = "admin";
  char senha[] = "ink";
  char c;
  int pos = 0;

	limpa_tela();
	
	pub_tela_aberta = login;
	cabecalho(login_c);
	pula_linha();
	if (errado == false)
	{
		text("Usuario ou senha incorretos!\n",false,false,tam_tela,true,centro,erro);
	}
	mensagem("\t\t\t\t\t\t\tUsuario: ", -1);
	scanf("%s", digito_usuario);
	getchar();
	password = getpass("\t\t\t\t\t\t\tPassword: ");
  
	limpar();
	if( !strcmp(password, senha) && !strcmp(digito_usuario, usuario) )
	{
  	abre_tela(menu);
	}
	else
  {
		if (0 != 0) //Aqui irá conectar ao banco caso não seja o ADM
		{
			abre_tela(menu);
		}
		else
		{
			loginTela(false);
		}
	}
}
