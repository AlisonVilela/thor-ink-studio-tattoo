#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "define.h"
#include "funcoes.h"
#include "terminal.h"

/***********************************************************/
/*																												 */
/*																												 */
/*                        TERMINAL INK          					 */
/*																												 */
/*																												 */
/***********************************************************/

void terminal()
{
	char *opcoes;
	char acao[tam];
	char msg[tam];
	Tacao r_acao;

	strcpy(msg,"\0");

	Ovoltar = false;

	if (Osair)
	{
		inicializa_sistema();
	}

	if (OpodeapagarCli)
	{
		cliPesq -> nome[0] = '\0';
		cliPesq -> rg[0] = '\0';
		cliPesq -> cpf[0] = '\0';
		cliPesq -> telefone[0] = '\0';
		cliPesq -> celular[0] = '\0';
		cliPesq = NULL;
		gravarCliente();
		Opesquisa = false;
		OpodeapagarCli = false;
		OapagarCli = false;
		Oopcoes = false;
	}

	if ((Osalvocomsucesso) && (pub_tela_aberta == cadastro))
	{
		text("Cliente salvo com sucesso.",false,false,tam,true,centro,concluido);
	}

	if ((Oeditadocomsucesso) && (pub_tela_aberta == cadastro))
	{
		text("Cliente editado com sucesso.",false,false,tam,true,centro,concluido);
	}

	if (Oerropesquisa)
	{
		text("Não foi encontrado nenhum cliente.",false,false,tam,true,centro,erro);
	}

	if (Omultpesquisa)
	{
		text("Foram encontrados alguns clientes semelhantes.\n",false,false,tam,true,esquerda,texto);
		text(pesqencontrado,false,false,strlen(pesqencontrado),true,esquerda,texto);
	}

	if (Oinvalidnome)
	{
		text("- Nome inválido.",false,false,tam,true,centro,erro);
	}
	if (Oinvalidrg)
	{
		text("- Rg inválido.",false,false,tam,true,centro,erro);
	}
	if (Oinvalidcpf)
	{
		text("- Cpf inválido.",false,false,tam,true,centro,erro);
	}
	if (Oinvalidtelefone)
	{
		text("- Telefone inválido.",false,false,tam,true,centro,erro);
	}
	if (Oinvalidcelular)
	{
		text("- Celular inválido.",false,false,tam,true,centro,erro);
	}
	
	Osalvocomsucesso = false;
	Oeditadocomsucesso = false;
	Oerropesquisa = false;
	Opesquisa = false;

	Omultpesquisa = false;
	pesqencontrado[0] = '\0';
	pesqencontrado_cont = 0;

	Oinvalidnome = false;
	Oinvalidrg = false;
	Oinvalidcpf = false;
	Oinvalidtelefone = false;
	Oinvalidcelular = false;

	if (Oinvalido)
	{
		text("OPÇÃO INVÁLIDA - digite \"help\" se estiver com dificuldades.\n",false,false,tam,true,centro,erro);
	}
	if (Ohelp)
	{
		text("O terminal ink é utilizado para facilitar sua navegação, nele você poderá abrir as telas desejadas e também",false,false,tam,true,esquerda,texto);
		text("cadastrar e mudar configurações totalmente por ele abaixo segue alguns comandos:",false,false,tam,true,esquerda,texto);
		pula_linha();
		text("* logout/sair",false,false,30,false,esquerda,texto);
		text("Utilizado para logoff do sistema(volta para a tela de login)",false,false,tam-30,true,esquerda,texto);
		text("* config",false,false,30,false,esquerda,texto);
		text("Utilizado para acessar as configurações do sistema, podendo alterar cores e outras coisas\n",false,false,tam-30,true,esquerda,texto);
		text("* (telas)",false,false,30,false,esquerda,texto);
		text("Para abrir as telas listadas basta colocar o nome dela",false,false,tam-30,true,esquerda,texto);
		text("* voltar",false,false,30,false,esquerda,texto);
		text("Apenas com alguma tela aberta. Volta para a tela anterior. Obs: não finaliza a atividade que está fazendo\n",false,false,tam-30,true,esquerda,texto);
		text("* help ++",false,false,30,false,esquerda,texto);
		text("Digite help e o nome da tela/função desejada para obter mais detalhes",false,false,tam-30,true,esquerda,texto);
		pula_linha();
	}
	Ohelp = false;

	if (Oagenda)
	{
		strcat(msg,"/agenda");
	}	
	if (Ocliente)
	{
		strcat(msg,"/clientes");
	}
	if (Ocad)
	{
		strcat(msg,"/cadastro");
	}
	if (Olista)
	{
		strcat(msg,"/lista_de_cliente");
	}
	if (OapagarCli)
	{
		text("Tem certeza que deseja apagar esse Cliente? [s][n]\n",false,false,tam,true,esquerda,texto);
		strcat(msg,"/apagar");		
	}
	if (Ologout)
	{
		text("Tem certeza que deseja sair? [s][n]\n",false,false,tam,true,esquerda,texto);
		strcat(msg,"/logout");
	}
	limpar();
	printf("\tInk%s: ", msg);
	fgets(acao, tam_tela, stdin);
	if (strlen(acao) > 0)
	{
		acao[strlen(acao)-1] = '\0';
	}

	opcoes = strtok(acao," ");

	while (opcoes != NULL)
	{
		r_acao = verifica_acao(opcoes);

		if (Oopcoes)
		{
			if (r_acao == sim)
			{
				if(Ologout)
				{
					Osair = true;
				}
				if(OapagarCli)
				{
					OpodeapagarCli = true;
					NovoCliente.nome[0] = '\0';
					NovoCliente.rg[0] = '\0';
					NovoCliente.cpf[0] = '\0';
					NovoCliente.telefone[0] = '\0';
					NovoCliente.celular[0] = '\0';
				}
			}else
			if (r_acao == nao)
			{
				if(Ologout)
				{
					Ologout = false;
					Osair = false;
					Oopcoes = false;
				}
				if(OapagarCli)
				{
					OpodeapagarCli = false;
					OapagarCli = false;
					Oopcoes = false;
				}
			}else
			{
				r_acao = invalido;
			}
		}

		if (r_acao != invalido)
		{
			if (Oopcoes == false)
			{
				if (r_acao == apagacli)
				{
					if (Opesquisa)
					{
						Oopcoes = true;
						OapagarCli = true;
						Oinvalido = false;
						Ohelp = false;
					}
					else
					{
						Ohelp = false;
						Opesquisa = false;
						Oinvalido = false;
						Opesquisa = false;
						NovoCliente.nome[0] = '\0';
						NovoCliente.rg[0] = '\0';
						NovoCliente.cpf[0] = '\0';
						NovoCliente.telefone[0] = '\0';
						NovoCliente.celular[0] = '\0';
					}
					break;
				}
				if (logout == r_acao)
				{
					Ohelp = false;
					Ologout = true;
					Oopcoes = true;
					Oinvalido = false;
					break;
				}
				if (help == r_acao)
				{
					Ohelp = true;
					Oopcoes = false;
					Oinvalido = false;
					break;
				}
			}
			switch (pub_tela_aberta)
			{
				case menu:
					{
						if (Ocliente)
						{
							Oinvalido = true;
							Ocliente = false;
							r_acao = invalido;
							break;
						}
						if (r_acao == voltar)
						{
							Oinvalido = true;
							Ocliente = false;
							r_acao = invalido;
							break;
						}
						if (r_acao == listar)
						{
							Oinvalido = true;
							Ocliente = false;
							r_acao = invalido;
							break;
						}
						if (r_acao == cadastrar)
						{
							Oinvalido = true;
							Ocliente = false;
							r_acao = invalido;
							break;
						}
						switch (r_acao)
						{
							case acliente:
								{
									Ohelp = false;
									Oopcoes = false;
									Oinvalido = false;
									Ocliente = true;
									break;
								}
							case tagenda:
								{
									Ohelp = false;
									Oopcoes = false;
									Oinvalido = false;
									Oagenda = true;
									break;
								}
						}
						break;
					}
				case cliente:
					{
						switch (r_acao)
						{
							case voltar:
								{
									Oinvalido = false;
									Ovoltar = true;
									break;
								}
							case cadastrar:
								{
									Oinvalido = false;
									Ocad = true;
									break;
								}
							case listar:
								{
									Oinvalido = false;
									Olista = true;
									break;
								}
						}
					}
				case cadastro:
					{
						switch (r_acao)
						{
							case voltar:
								{
									Oinvalido = false;
									Opesquisa = false;
									Ovoltar = true;
									NovoCliente.nome[0] = '\0';
									NovoCliente.rg[0] = '\0';
									NovoCliente.cpf[0] = '\0';
									NovoCliente.telefone[0] = '\0';
									NovoCliente.celular[0] = '\0';
									break;
								}
							case pesquisa:
								{
									Oinvalido = false;
									Opesquisa = true;
									opcoes = strtok(NULL, "\"");
									if (opcoes == NULL)
									{
										Oinvalido = false;
										break;
									}
									if (strlen(opcoes) > 50)
									{
										opcoes[50] = '\0';
									}
									cliPesq = pesquisaCli(&clientes, &opcoes);
									if (cliPesq == NULL)
									{
										Opesquisa = false;
										if (pesqencontrado_cont > 0)
										{
											Omultpesquisa = true;
										}
										else
										{
											Oerropesquisa = true;
										}
									}
									break;
								}
							case n:
								{
									Oinvalido = false;
									opcoes = strtok(NULL, "\"");
									if (opcoes == NULL)
									{
										Oinvalido = false;
										break;
									}
									if (strlen(opcoes) > 50)
									{
										opcoes[50] = '\0';
									}
									strcpy(NovoCliente.nome, opcoes);
									break;
								}
							case r:
								{
									Oinvalido = false;
									opcoes = strtok(NULL, " ");
									if (opcoes == NULL)
									{
										Oinvalido = false;
										break;
									}
									if (strlen(opcoes) > 12)
									{
										opcoes[12] = '\0';
									}
									strcpy(NovoCliente.rg, opcoes);
									break;
								}
							case c:
								{
									Oinvalido = false;
									opcoes = strtok(NULL, " ");
									if (opcoes == NULL)
									{
										Oinvalido = false;
										break;
									}
									if (strlen(opcoes) > 11)
									{
										opcoes[11] = '\0';
									}
									strcpy(NovoCliente.cpf, opcoes);
									break;
								}
							case t:
								{
									Oinvalido = false;
									opcoes = strtok(NULL, " ");
									if (opcoes == NULL)
									{
										Oinvalido = false;
										break;
									}
									if (strlen(opcoes) > 10)
									{
										opcoes[10] = '\0';
									}
									strcpy(NovoCliente.telefone, opcoes);
									break;
								}
							case ce:
								{
									Oinvalido = false;
									opcoes = strtok(NULL, " ");
									if (opcoes == NULL)
									{
										Oinvalido = false;
										break;
									}
									if (strlen(opcoes) > 11)
									{
										opcoes[11] = '\0';
									}
									strcpy(NovoCliente.celular, opcoes);
									break;
								}
							case limparcad:
								{
									Oinvalido = false;
									Opesquisa = false;
									NovoCliente.nome[0] = '\0';
									NovoCliente.rg[0] = '\0';
									NovoCliente.cpf[0] = '\0';
									NovoCliente.telefone[0] = '\0';
									NovoCliente.celular[0] = '\0';
									break;
								}
							case savecad:
								{
									Oinvalido = false;


									if (NovoCliente.nome[0] == '\0')
									{
										Oinvalidnome = true;
									}
									if (NovoCliente.rg[0] == '\0')
									{
										Oinvalidrg = true;
									}
									if (strlen(NovoCliente.cpf) != 11)
									{
										Oinvalidcpf = true;
									}
									if (strlen(NovoCliente.telefone) != 10)
									{
										Oinvalidtelefone = true;
									}
									if (strlen(NovoCliente.celular) != 11)
									{
										Oinvalidcelular = true;
									}																	

									if (Oinvalidnome || Oinvalidrg || Oinvalidcpf || Oinvalidtelefone || Oinvalidcelular)
									{
										break;
									}

									if (Opesquisa)
									{
										editaCliente(&cliPesq);
										gravarCliente();
										Oeditadocomsucesso = true;
									}
									else
									{
										salvarCliente(&clientes);
										gravarCliente();
										Osalvocomsucesso = true;
									}
									NovoCliente.nome[0] = '\0';
									NovoCliente.rg[0] = '\0';
									NovoCliente.cpf[0] = '\0';
									NovoCliente.telefone[0] = '\0';
									NovoCliente.celular[0] = '\0';
									break;
								}
							case novocli:
								{
									Ohelp = false;
									Opesquisa = false;
									Oinvalido = false;
									Opesquisa = false;
									NovoCliente.nome[0] = '\0';
									NovoCliente.rg[0] = '\0';
									NovoCliente.cpf[0] = '\0';
									NovoCliente.telefone[0] = '\0';
									NovoCliente.celular[0] = '\0';
									break;
								}
						}
						break;
					}
				case lista:
					{
						switch (r_acao)
						{
							case voltar:
								{
									Oinvalido = false;
									Ovoltar = true;
									break;
								}
						}
					}
				case agenda:
					{
						switch (r_acao)
						{
							case voltar:
								{
									Oinvalido = false;
									Ovoltar = true;
									break;
								}
						}
					}
			}
		}
		else
		{
			if (Oopcoes == false)
			{
				Osair = false;
				Oinvalido = true;
				Ohelp = false;
				Osair = false;
				Ovoltar = false;
			}
		}
		opcoes = strtok(NULL, " ");
	}

	if ((pub_tela_aberta == lista) && (Ovoltar == true))
	{
		Olista = false;
		abre_tela(cliente);
	}

	if ((pub_tela_aberta == cadastro) && (Ovoltar == true))
	{
		Ocad = false;
		abre_tela(cliente);
	}

	if ((pub_tela_aberta == cliente) && (Ovoltar == true))
	{
		Ocliente = false;
		abre_tela(menu);
	}

	if ((pub_tela_aberta == agenda) && (Ovoltar == true))
	{
		Oagenda = false;
		abre_tela(menu);
	}

	if ((pub_tela_aberta == cliente) && (Ocad == true))
	{
		abre_tela(cadastro);
	}

	if ((pub_tela_aberta == cliente) && (Olista == true))
	{
		abre_tela(lista);
	} else
	if ((pub_tela_aberta == menu) && (Ocliente == true))
	{
		abre_tela(cliente);
	} else
	if ((pub_tela_aberta == menu) && (Oagenda == true))
	{
		abre_tela(agenda);
	} else
	abre_tela(pub_tela_aberta);
}

Tacao verifica_acao(char *acao)
{
	if ((strcmp(modificatamanho(acao,maiusculo), "HELP")==0) || (strcmp(modificatamanho(acao,maiusculo), "H")==0))
	{
		return help;
	} else
	if ((strcmp(modificatamanho(acao,maiusculo), "AGORA")==0) || (strcmp(modificatamanho(acao,maiusculo), "A")==0))
	{
		return agora;
	} else
	if ((strcmp(modificatamanho(acao,maiusculo), "CLIENTE")==0) || (strcmp(modificatamanho(acao,maiusculo), "CLI")==0) || (strcmp(modificatamanho(acao,maiusculo), "C")==0))
	{
		if (pub_tela_aberta == menu)
		{
			return acliente;
		}
		else
		{
			return invalido;
		}
	} else
	if ((strcmp(modificatamanho(acao,maiusculo), "S")==0))
	{
		return sim;
	} else
	if ((strcmp(modificatamanho(acao,maiusculo), "N")==0))
	{
		return nao;
	} else
	if ((strcmp(modificatamanho(acao,maiusculo), "VOLTAR")==0) || (strcmp(modificatamanho(acao,maiusculo), "BACK")==0) || (strcmp(modificatamanho(acao,maiusculo), "V")==0))
	{
		return voltar;
	} else
	if ((strcmp(modificatamanho(acao,maiusculo), "LOGOUT")==0) || (strcmp(modificatamanho(acao,maiusculo), "SAIR")==0))
	{
		return logout;
	}	else
	if ((strcmp(modificatamanho(acao,maiusculo), "CAD")==0) || (strcmp(modificatamanho(acao,maiusculo), "CADASTRO")==0))
	{
		return cadastrar;
	}	else
	if ((strcmp(modificatamanho(acao,maiusculo), "LISTA")==0) || (strcmp(modificatamanho(acao,maiusculo), "L")==0))
	{
		return listar;
	}	else
	if ((strcmp(modificatamanho(acao,maiusculo), "-N")==0) || (strcmp(modificatamanho(acao,maiusculo), "NOME")==0))
	{
		return n;
	}	else
	if ((strcmp(modificatamanho(acao,maiusculo), "-R")==0) || (strcmp(modificatamanho(acao,maiusculo), "RG")==0))
	{
		return r;
	}	else
	if ((strcmp(modificatamanho(acao,maiusculo), "-C")==0) || (strcmp(modificatamanho(acao,maiusculo), "CPF")==0))
	{
		return c;
	}	else
	if ((strcmp(modificatamanho(acao,maiusculo), "-T")==0) || (strcmp(modificatamanho(acao,maiusculo), "TELEFONE")==0))
	{
		return t;
	}	else
	if ((strcmp(modificatamanho(acao,maiusculo), "-CE")==0) || (strcmp(modificatamanho(acao,maiusculo), "CELULAR")==0))
	{
		return ce;
	}	else
	if ((strcmp(modificatamanho(acao,maiusculo), "LIMPAR")==0) || (strcmp(modificatamanho(acao,maiusculo), "CLEAR")==0) || (strcmp(modificatamanho(acao,maiusculo), "CANCEL")==0) || (strcmp(modificatamanho(acao,maiusculo), "CANCELAR")==0))
	{
		return limparcad;
	}	else
	if ((strcmp(modificatamanho(acao,maiusculo), "SALVAR")==0) || (strcmp(modificatamanho(acao,maiusculo), "SAVE")==0))
	{
		return savecad;
	}	else
	if ((strcmp(modificatamanho(acao,maiusculo), "-P")==0) || (strcmp(modificatamanho(acao,maiusculo), "PESQUISAR")==0))
	{
		return pesquisa;
	}	else
	if ((strcmp(modificatamanho(acao,maiusculo), "-APAGA")==0) || (strcmp(modificatamanho(acao,maiusculo), "-APAGAR")==0))
	{
		return apagacli;
	}	else
	if ((strcmp(modificatamanho(acao,maiusculo), "NOVO")==0) || (strcmp(modificatamanho(acao,maiusculo), "-NOVO")==0))
	{
		return novocli;
	}	else
	if ((strcmp(modificatamanho(acao,maiusculo), "AGENDA")==0) || (strcmp(modificatamanho(acao,maiusculo), "AG")==0))
	{
		return tagenda;
	}
	else
	{
		return invalido;
	}
}

