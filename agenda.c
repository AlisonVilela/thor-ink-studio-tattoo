#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "define.h"
#include "funcoes.h"
#include "agenda.h"

/***********************************************************/
/*																												 */
/*																												 */
/*                 	 AGENDA - THOR INK 	  	  						 */
/*																												 */
/*																												 */
/***********************************************************/

void agendaTela(data_time SeDia)
{
	limpa_tela();
	char diaselecionado[40];

	strcpy(diaselecionado, "Dia: ");
	strcat(diaselecionado, datatostr(SeDia));

	cabecalho(padrao);
	text("",true,true,tam_tela,true,centro,texto);
	text("Agenda(Em Construcao)",true,true,tam_tela,true,centro,texto);
	linha(true, true,tam_tela, true);
	text("",true,true,50,false,centro,texto);
	text("",true,true,88,true,centro,texto);
	text("",true,true,50,false,centro,texto);
	text("",true,true,88,true,centro,texto);
	text("JANEIRO",true,true,50,false,centro,texto);
	text(diaselecionado,true,true,88,true,esquerda,texto);
	text("",true,true,50,false,centro,texto);
	text("",true,true,88,true,centro,texto);

	text("",true,false,6,false,centro,texto);
	text("DOM",false,false,3,false,centro,texto);
	text("SEG",false,false,3,false,centro,texto);
	text("TER",false,false,3,false,centro,texto);
	text("QUA",false,false,3,false,centro,texto);
	text("QUI",false,false,3,false,centro,texto);
	text("SEX",false,false,3,false,centro,texto);
	text("SAB",false,false,3,false,centro,texto);
	text("",false,true,7,false,centro,texto);
	text("8:00  - ",true,false,8,false,esquerda,texto);		
	text("",false,false,32,false,esquerda,texto);

	text("14:00 - ",false,false,8,false,esquerda,texto);
	text("",false,true,32,true,esquerda,texto);

	text("",true,false,6,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text("",false,true,7,false,centro,texto);
	text("9:00  - ",true,false,8,false,esquerda,texto);
	text("",false,false,32,false,esquerda,texto);
	text("15:00 - ",false,false,8,false,esquerda,texto);
	text("",false,true,32,true,esquerda,texto);

	text("",true,false,6,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text("",false,true,7,false,centro,texto);
	text("10:00 - ",true,false,8,false,esquerda,texto);
	text("",false,false,32,false,esquerda,texto);
	text("16:00 - ",false,false,8,false,esquerda,texto);
	text("",false,true,32,true,esquerda,texto);

	text("",true,false,6,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text("",false,true,7,false,centro,texto);
	text("11:00 - ",true,false,8,false,esquerda,texto);
	text("",false,false,32,false,esquerda,texto);
	text("17:00 - ",false,false,8,false,esquerda,texto);
	text("",false,true,32,true,esquerda,texto);

	text("",true,false,6,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text("",false,true,7,false,centro,texto);
	text("12:00 - ",true,false,8,false,esquerda,texto);
	text("",false,false,32,false,esquerda,texto);
	text("18:00 - ",false,false,8,false,esquerda,texto);
	text("",false,true,32,true,esquerda,texto);

	text("",true,false,6,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text(" 0",false,false,3,false,centro,texto);
	text("",false,true,7,false,centro,texto);
	text("13:00 - ",true,false,8,false,esquerda,texto);
	text("",false,false,32,false,esquerda,texto);
	text("19:00 - ",false,false,8,false,esquerda,texto);
	text("",false,true,32,true,esquerda,texto);

	text("",true,true,50,false,centro,texto);
	text("",true,true,88,true,centro,texto);

	linha(true, true,50, false);
	linha(true, true,88, true);
	pula_linha();
	limpar();
	terminal();
}
