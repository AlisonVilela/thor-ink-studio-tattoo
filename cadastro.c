#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "define.h"
#include "funcoes.h"
#include "cadastro.h"

/***********************************************************/
/*																												 */
/*																												 */
/*                 Cadastro - THOR INK 	  	  						 */
/*																												 */
/*																												 */
/***********************************************************/

void cadastroTela()
{
	limpa_tela();

	cabecalho(padrao);
	text("",true,true,tam_tela,true,centro,texto);
	text("Cadastro de Cliente",true,true,tam_tela,true,centro,texto);
	linha(true, true,tam_tela, true);
	text("",true,true,tam_tela,true,centro,texto);
	text("",true,true,tam_tela,true,centro,texto);

	text("Nome.................: ",true,false,tam_tela/2,false,direita,texto);
	text(NovoCliente.nome,false,true,(tam_tela/2)-2,true,esquerda,texto);
	text("RG...................: ",true,false,tam_tela/2,false,direita,texto);
	text(NovoCliente.rg,false,true,(tam_tela/2)-2,true,esquerda,texto);
	text("CPF..................: ",true,false,tam_tela/2,false,direita,texto);
	text(NovoCliente.cpf,false,true,(tam_tela/2)-2,true,esquerda,texto);
	text("Telefone.............: ",true,false,tam_tela/2,false,direita,texto);
	text(NovoCliente.telefone,false,true,(tam_tela/2)-2,true,esquerda,texto);
	text("Celular..............: ",true,false,tam_tela/2,false,direita,texto);
	text(NovoCliente.celular,false,true,(tam_tela/2)-2,true,esquerda,texto);

	text("",true,true,tam_tela,true,centro,texto);
	linha(true, true,tam_tela, true);
	pula_linha();
	limpar();
	terminal();
}
