/***********************************************************/
/*																												 */
/*																												 */
/*                      DEFINIÇÕES - GALA           			 */
/*																												 */
/*																												 */
/***********************************************************/

/***********************************************************/
/*							      	   Constantes           					 */
/***********************************************************/

#define preto "\033[0;30m"
#define vermelho "\033[0;31m"
#define verde "\033[0;32m"
#define marrom "\033[0;33m"
#define azul "\033[0;34m"
#define roxo "\033[0;35m"
#define ciano "\033[0;36m"
#define cinza_claro "\033[0;37m"
#define preto_acinzentado "\033[1;30m"
#define vermelho_claro "\033[1;31m"
#define verde_claro "\033[1;32m"
#define amarelo "\033[1;33m"
#define azul_claro "\033[1;34m"
#define roxo_claro "\033[1;35m"
#define ciano_claro "\033[1;36m"
#define branco "\033[1;37m"
#define tam_tela 140
#define tam 142


/***********************************************************/
/*												Numeradores 		      					 */
/***********************************************************/

typedef enum
{
	false,
	true
}boolean;

typedef enum
{
	help,
	agora,
	logout,
	invalido,
	acliente,
	sim,
	nao,
	voltar,
	cadastrar,
	n,
	r,
	c,
	t,
	ce,
	limparcad,
	savecad,
	pesquisa,
	listar,
	apagacli,
	novocli,
	tagenda
}Tacao;

typedef enum
{
	login,
	menu,
	cliente,
	cadastro,
	lista,
	agenda
}tela;

typedef enum
{
	Preto,
	Vermelho,
	Verde,
	Marrom,
	Azul,
	Roxo,
	Ciano,
	Cinza_Claro,
	Preto_Acinzentado,
	Vermelho_Claro,
	Verde_Claro,
	Amarelo,
	Azul_Claro,
	Roxo_Claro,
	Ciano_Claro,
	Branco
}Tcor;

typedef enum
{
	login_c,
	padrao
}tipo_cabecalho;

typedef enum
{
	esquerda,
	direita,
	centro
}Tajuste;

typedef enum
{
	maiusculo,
	minusculo
}Ttipotamanho;
/***********************************************************/
/*												Estruturas  		      					 */
/***********************************************************/

typedef struct Tdata_time
{
	int dia;
	int mes;
	int ano;
}data_time;

typedef struct Thora_time
{
	int sec;
	int min;
	int hora;
}hora_time;

typedef struct Tdata_hora
{
 data_time data;
 hora_time hora;
}data_hora;

typedef struct
{
	Tcor fonte_normal;
	Tcor fonte_destacada;
	Tcor fonte_erro;
	Tcor borda;
	Tcor fundo;
}Tpreferencia_usuario;

typedef struct Tentidade
{
	char nome[51];
	char rg[13];
	char cpf[12];
	char telefone[11];
	char celular[12];
	char tipo[3];
	char login[21];
	char senha[21];
	int	id_preferencia;
	Tpreferencia_usuario preferencia_usuario;
	struct Tentidade *Prox;
}Tentidade;

typedef struct Tagenda
{
	char desc[51];
	data_hora data;
	Tentidade *cli;
	struct Tagenda *Prox;
}Tagenda;

/***********************************************************/
/*												Variáveis   		      					 */
/***********************************************************/

Tcor erro;
Tcor texto;
Tcor concluido;
Tcor corfundolista;

char bordac[10];

Tagenda *agendat;
Tagenda NovaAgenda;
Tentidade *clientes;
Tentidade *cliPesq;
Tentidade NovoCliente;

char diadasemana[13];
char pesqencontrado[tam];
int pesqencontrado_cont;

data_hora dataatual;
/***********************************************************/
/*			Variáveis booleanas utilizadas pelo terminal			 */
/***********************************************************/
boolean Ohelp;
boolean Ologout;
boolean Oinvalido;
boolean Oopcoes;
boolean Ocliente;
boolean Osair;
boolean Ovoltar;
boolean Ocad;
boolean Olista;
boolean OapagarCli;
boolean Ofundolista;
boolean Oinvalidnome;
boolean Oinvalidrg;
boolean Oinvalidcpf;
boolean Oinvalidtelefone;
boolean Oinvalidcelular;
boolean Opesquisa;
boolean Oerropesquisa;
boolean Omultpesquisa;
boolean Osalvocomsucesso;
boolean Oeditadocomsucesso;
boolean OpodeapagarCli;
boolean Oagenda;

tela pub_tela_aberta;
